/*
 * HA-JDBC: High-Availability JDBC
 * Copyright (c) 2004-2007 Paul Ferraro
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published by the 
 * Free Software Foundation; either version 2.1 of the License, or (at your 
 * option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License 
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, 
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: ferraro@users.sourceforge.net
 */
package net.sf.hajdbc.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.hajdbc.Database;
import net.sf.hajdbc.DatabaseCluster;
import net.sf.hajdbc.DatabaseClusterFactory;
import net.sf.hajdbc.Messages;
import net.sf.hajdbc.util.SQLExceptionFactory;
import net.sf.hajdbc.util.Strings;
import net.sf.hajdbc.util.reflect.ProxyFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author  Paul Ferraro
 * @version $Revision: 2015 $
 */
public final class Driver implements java.sql.Driver
{
	private static final Pattern URL_PATTERN = Pattern.compile("jdbc:ha-jdbc:(.+)"); //$NON-NLS-1$
	private static final String CONFIG = "config"; //$NON-NLS-1$
	
	private static Logger logger = LoggerFactory.getLogger(Driver.class);
	
	static
	{
		try
		{
			DriverManager.registerDriver(new Driver());
		}
		catch (SQLException e)
		{
			logger.error(Messages.getMessage(Messages.DRIVER_REGISTER_FAILED, Driver.class.getName()), e);
		}
	}
	
	/**
	 * @see java.sql.Driver#acceptsURL(java.lang.String)
	 */
	@Override
	public boolean acceptsURL(String url)
	{
		return (this.parse(url) != null);
	}

	/**
	 * @see java.sql.Driver#connect(java.lang.String, java.util.Properties)
	 */
	@Override
	public Connection connect(String url, final Properties properties) throws SQLException
	{
		String id = this.parse(url);
		
		if (id == null) return null;
		
		DatabaseCluster<java.sql.Driver> cluster = this.getDatabaseCluster(id, properties);
		
		DriverInvocationHandler handler = new DriverInvocationHandler(cluster);
		
		java.sql.Driver driver = ProxyFactory.createProxy(java.sql.Driver.class, handler);
		
		Invoker<java.sql.Driver, java.sql.Driver, Connection> invoker = new Invoker<java.sql.Driver, java.sql.Driver, Connection>()
		{
			public Connection invoke(Database<java.sql.Driver> database, java.sql.Driver driver) throws SQLException
			{
				String url = ((DriverDatabase) database).getUrl();
				
				return driver.connect(url, properties);
			}
		};
		
		TransactionContext<java.sql.Driver> context = new LocalTransactionContext<java.sql.Driver>(cluster);
		
		try
		{
			return new ConnectionInvocationStrategy<java.sql.Driver, java.sql.Driver>(cluster, driver, context).invoke(handler, invoker);
		}
		catch (Exception e)
		{
			throw SQLExceptionFactory.createSQLException(e);
		}
	}

	/**
	 * @see java.sql.Driver#getMajorVersion()
	 */
	@Override
	public int getMajorVersion()
	{
		return Integer.parseInt(version()[0]);
	}
	
	/**
	 * @see java.sql.Driver#getMinorVersion()
	 */
	@Override
	public int getMinorVersion()
	{
		return Integer.parseInt(version()[1]);
	}

	private String[] version()
	{
		return DatabaseClusterFactory.getVersion().split(Strings.DASH)[0].split(Pattern.quote(Strings.DOT));
	}
	
	/**
	 * @see java.sql.Driver#getPropertyInfo(java.lang.String, java.util.Properties)
	 */
	@Override
	public DriverPropertyInfo[] getPropertyInfo(String url, final Properties properties) throws SQLException
	{
		String id = this.parse(url);
		
		if (id == null) return null;
		
		DatabaseCluster<java.sql.Driver> cluster = this.getDatabaseCluster(id, properties);
		
		DriverInvocationHandler handler = new DriverInvocationHandler(cluster);
		
		Invoker<java.sql.Driver, java.sql.Driver, DriverPropertyInfo[]> invoker = new Invoker<java.sql.Driver, java.sql.Driver, DriverPropertyInfo[]>()
		{
			public DriverPropertyInfo[] invoke(Database<java.sql.Driver> database, java.sql.Driver driver) throws SQLException
			{
				String url = ((DriverDatabase) database).getUrl();
				
				return driver.getPropertyInfo(url, properties);
			}			
		};
		
		try
		{
			return new DriverReadInvocationStrategy<java.sql.Driver, java.sql.Driver, DriverPropertyInfo[]>().invoke(handler, invoker);
		}
		catch (Exception e)
		{
			throw SQLExceptionFactory.createSQLException(e);
		}
	}

	/**
	 * @see java.sql.Driver#jdbcCompliant()
	 */
	@Override
	public boolean jdbcCompliant()
	{
		return true;
	}

	private DatabaseCluster<java.sql.Driver> getDatabaseCluster(String id, Properties properties) throws SQLException
	{
		DatabaseCluster<java.sql.Driver> cluster = DatabaseClusterFactory.getDatabaseCluster(id, DriverDatabaseCluster.class, DriverDatabaseClusterMBean.class, properties.getProperty(CONFIG));
		
		if (cluster == null)
		{
			throw new SQLException(Messages.getMessage(Messages.INVALID_DATABASE_CLUSTER, id));
		}
		
		return cluster;
	}
	
	private String parse(String url)
	{
		Matcher matcher = URL_PATTERN.matcher(url);
		
		if (!matcher.matches())
		{
			return null;
		}
		
		return matcher.group(1);
	}
}
