/*
 * HA-JDBC: High-Availability JDBC
 * Copyright (c) 2004-2007 Paul Ferraro
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published by the 
 * Free Software Foundation; either version 2.1 of the License, or (at your 
 * option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License 
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, 
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: ferraro@users.sourceforge.net
 */
package net.sf.hajdbc.sql;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.management.DynamicMBean;
import javax.management.NotCompliantMBeanException;
import javax.management.StandardMBean;

import net.sf.hajdbc.Messages;

/**
 * @author  Paul Ferraro
 * @version $Revision: 1777 $
 * @since   1.0
 */
public class DriverDatabase extends AbstractDatabase<Driver> implements InactiveDriverDatabaseMBean
{
	private static final String USER = "user"; //$NON-NLS-1$
	private static final String PASSWORD = "password"; //$NON-NLS-1$
	
	private String url;
	private Class<? extends Driver> driverClass;
	
	/**
	 * @see net.sf.hajdbc.sql.ActiveDriverDatabaseMBean#getUrl()
	 */
	@Override
	public String getUrl()
	{
		return this.url;
	}
	
	/**
	 * @see net.sf.hajdbc.sql.InactiveDriverDatabaseMBean#setUrl(java.lang.String)
	 */
	@Override
	public void setUrl(String url)
	{
		this.getDriver(url);
		this.checkDirty(this.url, url);
		this.url = url;
	}
	
	/**
	 * @see net.sf.hajdbc.sql.ActiveDriverDatabaseMBean#getDriver()
	 */
	@Override
	public String getDriver()
	{
		return (this.driverClass != null) ? this.driverClass.getName() : null;
	}
	
	/**
	 * @see net.sf.hajdbc.sql.InactiveDriverDatabaseMBean#setDriver(java.lang.String)
	 */
	@Override
	public void setDriver(String driver)
	{
		try
		{
			Class<? extends Driver> driverClass = null;
			
			if ((driver != null) && (driver.length() > 0))
			{
				driverClass = Class.forName(driver).asSubclass(Driver.class);
			}
			
			this.checkDirty(this.driverClass, driverClass);
			this.driverClass = driverClass;
		}
		catch (ClassNotFoundException e)
		{
			throw new IllegalArgumentException(e);
		}
		catch (ClassCastException e)
		{
			throw new IllegalArgumentException(e);
		}
	}
	
	/**
	 * @see net.sf.hajdbc.Database#connect(java.lang.Object)
	 */
	@Override
	public Connection connect(Driver driver) throws SQLException
	{
		Properties properties = new Properties(this.getProperties());

		String user = this.getUser();
		
		if (user != null)
		{
			properties.setProperty(USER, user);
		}

		String password = this.getPassword();
		
		if (password != null)
		{
			properties.setProperty(PASSWORD, password);
		}
		
		return driver.connect(this.url, properties);
	}

	/**
	 * @see net.sf.hajdbc.Database#createConnectionFactory()
	 */
	@Override
	public Driver createConnectionFactory()
	{
		return this.getDriver(this.url);
	}

	private Driver getDriver(String url)
	{
		try
		{
			return DriverManager.getDriver(url);
		}
		catch (SQLException e)
		{
			throw new IllegalArgumentException(Messages.getMessage(Messages.JDBC_URL_REJECTED, url), e);
		}
	}

	/**
	 * @see net.sf.hajdbc.Database#getActiveMBean()
	 */
	@Override
	public DynamicMBean getActiveMBean()
	{
		try
		{
			return new StandardMBean(this, ActiveDriverDatabaseMBean.class);
		}
		catch (NotCompliantMBeanException e)
		{
			throw new IllegalStateException(e);
		}
	}

	/**
	 * @see net.sf.hajdbc.Database#getInactiveMBean()
	 */
	@Override
	public DynamicMBean getInactiveMBean()
	{
		try
		{
			return new StandardMBean(this, InactiveDriverDatabaseMBean.class);
		}
		catch (NotCompliantMBeanException e)
		{
			throw new IllegalStateException(e);
		}
	}
}
